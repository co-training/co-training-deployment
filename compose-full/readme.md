# Docker compose full

In this section, we will explain how to run the application (frontend and backend) into a docker compose environment using the `postgres` database. Using this configuration you can use the application without losing data.

## Clean

You can start by removing any existing container :

```shell
$ docker rm -f $(docker ps -a -q)
```

## Start

```shell
$ docker-compose up -d
```

The first time you run this command the spring app will not probably work, and you may have a database connection error.
In this case, you have to wait for the database to start you can check the logs of the postgres server :

```shell
$ docker-compose logs -f postgres-service
```

Wait for this line :

```shell
postgres            | LOG:  database system is ready to accept connections
```

After starting the database, you have to run the schema script to install the app tables. Go to adminer :

- http://localhost:8000

<div align="center">
![1](/uploads/8d22c651459255f5af80750e6e9ce38d/1.png)
</div>

the password is : password

Then go to sql command :

<div align="center">
![2](/uploads/d9fc14e84ec232e5defe65eb893c61a9/2.png)
</div>

Now you can run the postgres schema script schema-postgres.sql which you can find it here.

- https://gitlab.com/co-training/co-training-backend/-/blob/master/co-training-rest/src/main/resources/schema-postgres.sql

<div align="center">
![3](/uploads/9b15816e362614d532333c3d7153e241/3.png)
</div>

Execute :

<div align="center">
![4](/uploads/caee64ff5b39d475ab557fb906e6ddb2/4.png)
</div>

As we have no data yet in the app, you have to create at lease an admin user to start. The queries below will create a new one. Run it like you did for the schema.

```sql
-- Team table
insert into public.team (id, name) values (8,'b-team');

-- First User : Administrator
insert into user_resource (id, budget, jackpot, nb_days) values (99, 0.0, 0.0, 0);

insert into public.user
(birthday, docker_hub, is_docker_hub_enabled, first_name, job, last_name, team_id, user_resource_id, email, is_email_enabled, photo) values
('2004-12-27', 'https://hub.docker.com/java', TRUE, 'Michel', 'BUTCHER','DUBOIS', 8, 99, 'admin@mail.com', FALSE, 'https://cdn4.vectorstock.com/i/thumb-large/52/38/avatar-icon-vector-11835238.jpg');

insert into public.user_account (id, email, password, creation_date, is_active, request_date, is_new, user_id) values
(99, 'admin@mail.com', '$2a$10$0jI/67brDBfj2xxMnhi9V.1hPrtXZGEbxGZop.DEuGczGZyHLCWW.', CURRENT_DATE, TRUE, CURRENT_DATE, FALSE, 99);

insert into public.user_role (id, email, role, user_account_id) values (99, 'admin@mail.com', 'ROLE_ADMIN', 99);
```

<div align="center">
![5](/uploads/b4881f926265302a223ee7a643365de5/5.png)
</div>

restart the backend using this command :

```shell
$ docker-compose restart co-training-rest
```

Check the backend documentation to know more about this config :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/co-training-rest#run-the-app-on-a-docker-environment

If you have already a postgres database installed on your local machine, you can configure it to be used from the docker compose :

- https://stackoverflow.com/questions/53690702/access-host-machine-in-docker-container-on-ubuntu-18-04

- Admin user :

| email          | password | roles |
| -------------- | -------- | ----- |
| admin@mail.com | password | ADMIN |

## Test

You can test if the backend is available using curl. Here is an example :

```shell
$ curl http://localhost:4444/co-training/v1/teams
```

```json
[{ "id": 8, "name": "b-team" }]
```

Run your browser on disable-security mode as we are using the same origin (localhost):

```shell
$ google-chrome --disable-web-security --user-data-dir=${YOUR_HOME_TMP_DIRECTORY}
```

You can now access the app from :

- http://localhost:5000/

<div align="center">
![6](/uploads/4c340d3e83c065477f4919355ab45805/6.png)
</div>

<div align="center">
![7](/uploads/474055673c3038faf8aa7e29f593fa0e/7.png)
</div>

## Stop

To stop the created containers use this command :

```shell
$ docker-compose down --remove-orphans
```
