# Kubernetes stage

In this directory, you'll find the yaml files to install the the `co-training` on a kubernetes cluster. The main idea is the same as the `docker-full`. We will run the application (frontend and backend) using a relational database which is postgres in our case. This is the full configuration of the app which needs many steps to follow to make it available on a cloud service provider.

The cluster creation and configuration will not be shown here as i suppose that you have already a cluster. If you are a beginner, you can check how to do it in the backend documentation :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s

And you can check this repo to know more about kubernetes :

- https://gitlab.com/kubernetes-samples

Architecture :

- https://gitlab.com/co-training/co-training-gui/-/tree/master/k8s#architecture

## Database and secrets

If you are using `google cloud`, you can follow these steps to create the app database :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s#create-a-postgres-database

and to install the schema :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s#install-the-app-schema

After that, you have to add the secrets :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s#add-kubernetes-secrets

And finally, to add a `cloud sql proxy` :

- https://gitlab.com/co-training/co-training-backend/-/tree/master/k8s#add-cloud-sql-proxy

Please make sure to adapt the commands to your namespace. If your are not using `google cloud`, the idea will be the same and the steps will be a little different on other cloud providers.

Don't forget to add a first user as we did in the `compose-full` :

- https://gitlab.com/co-training/co-training-deployment/-/tree/master/compose-full#start

## Nginx

As we will use ingress to deploy all our applications, we have to install it on the cluster. You can consult this link for more details :

- https://kubernetes.github.io/ingress-nginx/deploy/#gce-gke

In my case, the last version of nginx install script was :

```shell script
$ kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.0/deploy/static/provider/cloud/deploy.yaml
```

Check if the ingress controller pods have started :

```shell script
$ kubectl get pods --namespace ingress-nginx
```

```log
NAMESPACE       NAME                                       READY   STATUS      RESTARTS   AGE
ingress-nginx   ingress-nginx-admission-create-krnxz       0/1     Completed   0          42s
ingress-nginx   ingress-nginx-admission-patch-l9l5t        0/1     Completed   0          42s
ingress-nginx   ingress-nginx-controller-58459d46f-5l8fz   1/1     Running     0          43s
```

## Get the ingress url

You can get your ingress url from your cloud provider. From gcloud for example, you can find it here :

![1](/uploads/64267d9137b37360e8bf45a866f82602/1.png)

or using command line :

```shell script
$ kubectl describe service -n ingress-nginx
```

![2](/uploads/070554f3f740e29f648e16540c3de2b1/2.png)

As you can see, my ingress url is : `35.195.92.235`

## Install the app

If you check the frontend docs :

- https://gitlab.com/co-training/co-training-gui/-/tree/master/docs

You'll understand that the frontend configuration part is a little delicate. Actually the frontend is js app so there is no environment variables to configure the backend apis url at runtime. The solution was to use was to use env variables at the build time. At the generation of the distribution process, we will get variables from pipeline and replace them in the js files. Then we can build the docker image and push it to docker hub.

As i said the docker image is pointing to my backend server, to modify that you can this configuration in the frontend deployment object :

```yaml
lifecycle:
  postStart:
    exec:
      command:
        [
          '/bin/sh',
          '-c',
          "sed -i 's/35.241.245.0/${INGRESS_SERVER_URL}/g' /usr/share/nginx/html/js/app.8a28d7d3.js",
        ]
```

This command will be executed after starting the pod. Just change `${INGRESS_SERVER_URL}` with your real server url. For more details check this post :

- https://stackoverflow.com/questions/44140593/how-to-run-command-after-initialization

Now, You can simply deploy the application using this command (from the main directory) :

```shell script
$ kubectl apply -f ./k8s-stage
```

## Tests

You can now access the app from :

- http://35.195.92.235/frontend

## Application users

Admin user :

| email          | password | roles |
| -------------- | -------- | ----- |
| admin@mail.com | password | ADMIN |

## Clean up

To stop the created containers use this command :

```shell
$ kubectl delete -f ./k8s-stage
$ kubectl delete -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v0.41.0/deploy/static/provider/cloud/deploy.yaml
```

Don't forget to delete the database.
