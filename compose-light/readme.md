# Docker compose light

In this area, we will explain how to run the application (frontend and backend) as docker compose without using any database configuration (by default the backend uses an in memory database h2). This is the best way to test the app quickly and to have a look at the major features that the app provides.

## Clean

You can start by removing any existing container :

```shell
$ docker rm -f $(docker ps -a -q)
```

## Start

```shell
$ docker-compose up -d
```

You can list containers using this command :

```shell
$ docker-compose ps
```

And access one of the containers using :

```shell
$ docker exec -it [container_name] sh
```

To check the logs :

```shell
$ docker logs [container_name]
```

## Test

Run your browser on disable-security mode as we are using the same origin (localhost):

```shell
$ google-chrome --disable-web-security --user-data-dir=${YOUR_HOME_TMP_DIRECTORY}
```

You can now access the app from :

- http://localhost:5000/

<div align="center">
![Selection_391](/uploads/02c01b5ea42d443cd3834d8843125f4a/Selection_391.png)
</div>

<div align="center">
![success](/uploads/a639c5102d0c80321988b82f1353abf5/success.png)
</div>

## Application users

If you are using the demo (backend profile is h2), here's the default users that you can use to test the app :

| email          | password | roles                                                          |
| -------------- | -------- | -------------------------------------------------------------- |
| admin@mail.com | password | ADMIN                                                          |
| jane@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |
| user@mail.com  | password | TRAININGS_READER, TRAININGS_WRITER, USERS_READER, USERS_WRITER |

## Stop

To stop the created containers use this command :

```shell
$ docker-compose down --remove-orphans
```
