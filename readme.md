# Company training deployment

This project contains the deployment config files of the co-training project.

This is the deployment part of the project and you can find other components here.

- UI project: https://gitlab.com/co-training/co-training-gui
- Backend project: https://gitlab.com/co-training/co-training-backend
- Ops project : https://gitlab.com/co-training/co-training-deployment

## Compose files

I've provided the docker compose files of the co training project so you can run the app as docker containers.

**compose full** : co training project using the full configuration which means using frontend, backend and a database.

**compose full** : co training project using the light configuration which means using a frontend and a backend.

## K8s files

I've provided the kubernetes yaml files of the co training project so you can run the app on the cloud.

**k8s production** : co training project using the full configuration which means using frontend, backend, sql proxy and a database.

**k8s stage** : co training project using the light configuration which means using a frontend and a backend.

## Demo

You can check the demo to know about how to use the app :

- https://gitlab.com/co-training/co-training-gui/-/blob/master/docs/screens.md

## References

- [docker ps](https://docs.docker.com/engine/reference/commandline/ps/)
- [docker compose](https://docs.docker.com/compose/reference/overview/)
- [docker volume rm](https://docs.docker.com/engine/reference/commandline/volume_rm/)
- [how-to-run-command-after-initialization](https://stackoverflow.com/questions/44140593/how-to-run-command-after-initialization)
- [Attach Handlers to Container Lifecycle Events](https://kubernetes.io/docs/tasks/configure-pod-container/attach-handler-lifecycle-event/)
- [Define a Command and Arguments for a Container](https://kubernetes.io/docs/tasks/inject-data-application/define-command-argument-container/)
- [Init Containers](https://kubernetes.io/fr/docs/concepts/workloads/pods/init-containers/)

## Authors

- **Amdouni Mohamed Ali** [[github](https://github.com/mouhamed-ali)]

![co-training](/uploads/d539c1c9156c1f14e2ae519af84afaba/co-training.png)
